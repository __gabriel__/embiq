from django.shortcuts import render
from .tasks import fetch_rate
from .models import Ask, Bid, BTCRate, Commission
from django.http import JsonResponse
from django.http import Http404, HttpResponse


def history(request):
    if request.method == "GET":
        start_date = request.GET.get('start_date', "none")
        end_date = request.GET.get('end_date', "none")
        btc_rate = BTCRate.objects.all().order_by('-timestamp')
        if start_date != "none":
            btc_rate = btc_rate.filter(timestamp__gte=start_date)
        if end_date != "none":
            btc_rate = btc_rate.filter(timestamp__lte=end_date)
        btc_rate = list(btc_rate.values('rate', 'timestamp'))
        data = {"btc_history": btc_rate}
        return JsonResponse(data, safe=False)
    else:
        raise Http404()


def set_commission(request, value):
    if request.method == "GET":
        commissions = Commission.objects.all()
        if commissions:
            commission = commissions.first()
            commission.value = value
            commission.save()
        else:
            Commission(value=value).save()
        return HttpResponse(status=200)
    else:
        raise Http404()


def orderbook(request):
    if request.method == "GET":
        limit = int(request.GET.get('limit', 50))
        if limit > 100:
            limit = 100
        price_from = request.GET.get('price_from', 0)
        price_to = request.GET.get('price_to', "none")
        # if given price with commission filter reglar filter is being turned off
        price_with_commission_from = request.GET.get('price_with_commission_from', 0)
        price_with_commission_to = request.GET.get('price_with_commission_to', "none")
        if price_with_commission_from != 0 or price_with_commission_to != "none":
            # setting default in case price with and without commission filters are being used
            price_from = 0
            price_to = "none"
        if price_to != "none":
            bids = Bid.objects.filter(price__gte=float(price_from), price__lte=float(price_to))
            bids.order_by('-id')[:int(limit)]
            asks = Ask.objects.filter(price__gte=float(price_from), price__lte=float(price_to))
            asks = asks.order_by('-id')[:int(limit)]
        else:
            bids = Bid.objects.filter(price__gte=float(price_from)).order_by('-id')[:int(limit)]
            asks = Ask.objects.filter(price__gte=float(price_from)).order_by('-id')[:int(limit)]
        commission = Commission.objects.all().first()
        if commission:
            value = commission.value
        else:
            value = 0
        bids = list(bids.values('price', 'amount', 'utc_timestamp'))
        for b in bids:
            price = b.get('price') - value
            if price_with_commission_to != "none":
                # using price with commission   s filter
                if float(price_with_commission_from) <= price <= float(price_with_commission_to):
                    b['price_with_commission'] = price
            else:
                if float(price_with_commission_from) <= price:
                    b['price_with_commission'] = price
        asks = list(asks.values('price', 'amount', 'utc_timestamp'))
        for a in asks:
            price = a.get('price') + value
            if price_with_commission_to != "none":
                # using price with commissions filter
                if float(price_with_commission_from) <= price <= float(price_with_commission_to):
                    a['price_with_commission'] = price
            else:
                if float(price_with_commission_from) <= price:
                    a['price_with_commission'] = price
        asks = [a for a in asks if len(a) == 4][:int(limit)]
        bids = [b for b in bids if len(b) == 4][:int(limit)]
        data = {'bids': bids,
                'asks': asks}
        return JsonResponse(data, safe=False)
    else:
        raise Http404()

from django.contrib import admin
from .models import Ask, Bid, BTCRate, Commission
# Register your models here.


admin.site.register(Ask)
admin.site.register(Bid)
admin.site.register(BTCRate)
admin.site.register(Commission)

from django.db import models


class Ask(models.Model):
    price = models.FloatField()
    amount = models.FloatField()
    utc_timestamp = models.DateTimeField()


class Bid(models.Model):
    price = models.FloatField()
    amount = models.FloatField()
    utc_timestamp = models.DateTimeField()


class BTCRate(models.Model):
    rate = models.FloatField()
    timestamp = models.DateTimeField()


class Commission(models.Model):
    value = models.FloatField()
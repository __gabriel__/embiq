from __future__ import absolute_import, unicode_literals
from celery import shared_task
import requests
from .models import Ask, Bid, BTCRate
from .utils import extract_utc


@shared_task(name='fetch_rate')
def fetch_rate():
    url = "https://api.bitfinex.com/v1/book/btcusd/?group=1&limit_bids=100&limit_asks=100"
    response = requests.request('GET', url)
    data = response.json()
    Bid.objects.all().delete()
    for bid in data['bids']:
        time = bid['timestamp']
        date = extract_utc(time)
        bid = Bid(price=bid['price'], amount=bid['amount'], utc_timestamp=date)
        bid.save()
    Ask.objects.all().delete()
    max_ask = data['asks'][-1]
    date = extract_utc(max_ask['timestamp'])
    BTCRate(rate=max_ask['price'], timestamp=date).save()
    for ask in data['asks']:
        time = ask['timestamp']
        date = extract_utc(time)
        ask = Ask(price=ask['price'], amount=ask['amount'], utc_timestamp=date)
        ask.save()
    return 0

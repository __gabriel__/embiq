import datetime


def extract_utc(time):
    date = datetime.datetime.utcfromtimestamp(int(time.split(".")[0]))
    return date
from django.conf.urls import url
from django.contrib import admin
from BTC.views import history, set_commission, orderbook


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/set_commission/(?P<value>\d+\.\d+)/', set_commission),
    url(r'^orderbook/', orderbook),
    url(r'^history/', history),
]

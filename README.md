README

Python3 required!

1 clone repo

2 create virtualenv

3 activate env

4 pip install -r requirements.txt

5 install and run redis (broker for celery): https://redis.io/topics/quickstart 

6 install and run postgresql

7 create database and psql user properly: https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-14-04 
 
8 configure DATABASES dictionary in setting.py ( help in previous link )

9 with env on and in USD_BTC folder run migrations: https://docs.djangoproject.com/en/1.11/topics/migrations/

10 in new terminal with env on and in USD_BTC folder:
   celery -A USD_BTC worker -l info   //worker to perform tasks

8)in new terminal with env on and in USD_BTC folder:
   celery -A USD_BTC beat    // process taking care of queueing tasks

9)in USD_BTC folder with env on:
   python manage.py runserver
-----------------------------------
Application is downloading and transforming BTC data every 30 seconds.

How to use:

GET  /admin -> django admin default view

GET /api/set_commission/<float value>
	endpoint sets commission value
	parameter value needs to be float (ex  2.4, 4.0)

GET /orderbook/
	returns orderbook
	optional parameters:
	
		-limit - sets how many records are being displayed, default 50
		
		-price_from - filter records - price field is >= "price_from"
		
		-price_to - filter records - price field is <= "price_to"
		
		-price_with_commission_from - price_with_commission field >= "price_with_commission_from"
		
		-price_with_commission_to - price_with_commission field <= "price_with_commission_to"
		
		Parameters can be combined but last two overrides price_from and _to
		
GET /history/
	returns history of BTC rates
	optional parameters:
	
		-start_date - filter records - date field is >= "start_date"
		
		-end_date - filter records - date field is <= "end_date"
		
		format of these parameters is: "YYYY-MM-DDTHH:mm:ssZ"
		
		So it is possible to filter only by years by doing for example /?start_date=2017
		
		or by year and month by doing /?start_date=2017-11
		and so on.
		
		Parameters can be combined.